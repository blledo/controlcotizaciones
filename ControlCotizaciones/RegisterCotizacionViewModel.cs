﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Data.SqlClient;
using System.IO;
using Dapper;
using Dapper.Contrib.Extensions;
using ClosedXML.Excel;

namespace ControlCotizaciones
{
    public class RegisterCotizacionViewModel : INotifyPropertyChanged
    {
        private Cotizacion _currentCotizacion { get; set; }
        public Cotizacion CurrentCotizacion
        {
            get => _currentCotizacion;
            set
            {
                _currentCotizacion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Booleano que determina si es una cotización a añadir o una a modificar
        /// </summary>
        public bool IsNew { get; set; } = true;

        /// <summary>
        /// Colección de instituciones para el combobox.
        /// </summary>
        public ObservableCollection<Institucion> InstitucionCollection { get; set; }
        public ObservableCollection<Solicitante> SolicitanteCollection { get; set; }
        public ObservableCollection<Estado> EstadoCollection { get; set; }
        public ObservableCollection<Responsable> ResponsableCollection { get; set; }


        private SqlConnection _sqlConnection { get; set; } = new SqlConnection()
        {
            ConnectionString = Properties.Settings.Default.Database
        };

        public RegisterCotizacionViewModel()
        {
            CurrentCotizacion = new Cotizacion();
            // Cargamos las colecciones por primera vez;
            RefreshCollections();
        }

        /// <summary>
        /// Refresca las colecciones de elementos para el autocompletado
        /// </summary>
        public void RefreshCollections()
        {

            // Usamos dapper
            InstitucionCollection = new ObservableCollection<Institucion>(
                _sqlConnection.Query<Institucion>("SELECT * FROM ControlCotizaciones.dbo.Institucion").ToList());

            SolicitanteCollection = new ObservableCollection<Solicitante>(
                _sqlConnection.Query<Solicitante>("SELECT * FROM ControlCotizaciones.dbo.Solicitante").ToList());

            EstadoCollection = new ObservableCollection<Estado>(
                _sqlConnection.Query<Estado>("SELECT * FROM ControlCotizaciones.dbo.Estado").ToList());

            ResponsableCollection = new ObservableCollection<Responsable>(
                _sqlConnection.Query<Responsable>("SELECT * FROM ControlCotizaciones.dbo.Responsable").ToList());
        }

        /// <summary>
        /// Este método recibe el nombre de archivo de una cotización existente, busca si está asociado a un registro de cotización y despliega la información de esta. Si no lo está,
        /// rellana la información en base al archivo, para registrarse como cotización nueva
        /// </summary>
        /// <param name="fileName"></param>
        public void CotizacionInfoFromFile(string fileName)
        {
            Cotizacion cotizacion = _sqlConnection.Query<Cotizacion, Institucion, Solicitante, Estado, Responsable, Cotizacion>(
                @"SELECT C.IdCotizacion, C.Nombre, C.FechaRecepcion, C.FechaInicio, C.FechaTermino,
                I.IdInstitucion, I.Nombre,
                S.IdSolicitante, S.Nombre,
                E.IdEstado, E.Nombre,
                R.IdResponsable, R.Nombre
                FROM Cotizacion C 
                LEFT JOIN Institucion I ON C.IdInstitucion = I.IdInstitucion
                LEFT JOIN Solicitante S ON C.IdSolicitante = S.IdSolicitante
                LEFT JOIN Estado E ON C.IdEstado = E.IdEstado
                LEFT JOIN Responsable R ON C.IdResponsable = R.IdResponsable
                WHERE C.IdCotizacion IN (SELECT TOP 1 IdCotizacion FROM Archivo WHERE Filename = @Filename)",
                map: (c, i, s, e, r) =>
                {
                    
                    /*
                     * Puede parecer enredado, pero lo que hacemos acá con cada atributo es revisar si existe en la colección de objetos/atributo que tenemos
                     * si existe, lo asignamos al POCO de cotización, para desplegarlo en la visa. Si no existe (que deberían ser casos muy, muy escasos, pero posibles) es porque
                     * otro usuario creó un nuevo registro de atributo de objeto que no se ha rescatado de la base de datos aún. En ese caso, se refrescan las colecciones hasta que lo tengamos
                     * 
                     */
                    if (i != null)
                    {
                        c.Institucion = InstitucionCollection
                            .SingleOrDefault(x => x.IdInstitucion == i.IdInstitucion);
                        
                        while(c.Institucion == null) // Lo reintentamos hasta que funcione
                        {
                            RefreshCollections();
                            c.Institucion = InstitucionCollection
                                .SingleOrDefault(x => x.IdInstitucion == i.IdInstitucion);
                        }                        
                    }

                    if (s != null)
                    {
                        c.Solicitante = SolicitanteCollection
                            .SingleOrDefault(x => x.IdSolicitante == s.IdSolicitante);
                    
                        while(c.Solicitante == null && s.IdSolicitante != null) // Lo reintentamos hasta que funcione
                        {
                            RefreshCollections();
                            c.Solicitante = SolicitanteCollection
                                .SingleOrDefault(x => x.IdSolicitante == s.IdSolicitante);
                        }
                    }

                    if (e != null)
                    {
                        c.Estado = EstadoCollection
                            .SingleOrDefault(x => x.IdEstado == e.IdEstado);
                    
                        while(c.Estado == null && e.IdEstado != null) // Lo reintentamos hasta que funcione
                        {
                            RefreshCollections();
                            c.Estado = EstadoCollection
                                .SingleOrDefault(x => x.IdEstado == e.IdEstado);
                        }                       
                    }

                    if (r != null)
                    {
                        c.Responsable = ResponsableCollection
                            .SingleOrDefault(x => x.IdResponsable == r.IdResponsable);
                    
                        while(c.Responsable == null && r.IdResponsable != null) // Lo reintentamos hasta que funcione
                        {
                            RefreshCollections();
                            c.Responsable = ResponsableCollection
                                .SingleOrDefault(x => x.IdResponsable == r.IdResponsable);
                        }
                        
                    }

                    return c;
                },
                splitOn: "IdInstitucion, IdSolicitante, IdEstado, IdResponsable",
                param: new
                {
                    Filename = fileName
                }).SingleOrDefault();

            if (cotizacion != null) // Este archivo ya está registrado a una cotización
            {
                IEnumerable<Archivo> archivosCotizacion = _sqlConnection.Query<Archivo>(
                    "SELECT * FROM Archivo WHERE IdCotizacion = @IdCotizacion",
                    new
                    {
                        cotizacion.IdCotizacion
                    });

                cotizacion.Archivos = new ObservableCollection<Archivo>(archivosCotizacion);
                
                CurrentCotizacion = cotizacion;
                
                return;
            }

 
            DirectoryInfo di = new DirectoryInfo(Path.GetDirectoryName(fileName) ?? throw new InvalidOperationException("El filename es nulo"));

            CurrentCotizacion.Nombre = Path.GetFileNameWithoutExtension(fileName);

            // Muy importante: en el caso de que estos objetos/atributos no existan en la base de datos, es necesario agregarlos al ObservableCollection
            // de este ViewModel, y LUEGO asignarlos a la propiedad, porque los métodos del binding en WPF desestiman el nuevo objeto si no pertenece al ItemsSource
            // del control correspondiente (en el caso de los ComboBox, al menos)
                
            // Buscamos si existe, si no creamos un objeto institución sin id (una institución no registrada que se guarda y asigna un ID si se registra la cotización)

            Institucion institucion = InstitucionCollection.SingleOrDefault(i => i.Nombre.Equals(di.Name.ToUpper()));

            if (institucion == null)
            {
                institucion = new Institucion(di.Name.ToUpper());
                InstitucionCollection.Add(institucion);
            }

            CurrentCotizacion.Institucion = institucion;  
            
            // Creamos un nuevo objeto Archivo y le damos los atributos de este archivo
            
            // Como los archivos suelen ser excel, podemos intentar abrirlo con ClosedXML para contar las lineas

            IXLWorkbook xlWorkbook = null;

            int lines = 0;

            try
            {
                xlWorkbook = new XLWorkbook(fileName);

                lines = xlWorkbook.Worksheet(1).RowsUsed().Count();
            }
            catch
            {
                // Si hay cualqueir problema intentando cargarlo como libro de Excel, ignoramos y continuamos
            }



            List<Archivo> archivos = new List<Archivo>();

            archivos.Add(new Archivo()
            {
                Nombre = Path.GetFileName(fileName),
                Filename = fileName,
                Lineas = lines,
                Cotizacion = CurrentCotizacion
            });
            
            CurrentCotizacion.Archivos = new ObservableCollection<Archivo>(archivos);
        }
        public void Submit()
        {
            SqlMapperExtensions.TableNameMapper = (type) => {
                // Configuramos Dapper para que use los nombres de tablas idénticos a los objetos que les insertamos
                return type.Name;
            };

            // Los atributos de la cotización sin ID son nuevos, asi que se deben insertar como nuevos registros en la base de datos
            // El método .Insert de Dapper.Contrib nos devuelve el ID autoincrementado que se le asigna al nuevo registro
            
            
            if (CurrentCotizacion.Institucion.IdInstitucion == null)
            {
                CurrentCotizacion.Institucion.IdInstitucion = _sqlConnection.Insert(CurrentCotizacion.Institucion);
            }

            if(CurrentCotizacion.Estado.IdEstado == null)
            {
                CurrentCotizacion.Estado.IdEstado = _sqlConnection.Insert(CurrentCotizacion.Estado);
            }


            if (CurrentCotizacion.Solicitante != null && CurrentCotizacion.Solicitante.IdSolicitante == null)
            {
                CurrentCotizacion.Solicitante.IdSolicitante = _sqlConnection.Insert(CurrentCotizacion.Solicitante);
            }

            if(CurrentCotizacion.Responsable != null && CurrentCotizacion.Responsable.IdResponsable == null )
            {
                CurrentCotizacion.Responsable.IdResponsable = _sqlConnection.Insert(CurrentCotizacion.Responsable);

            }

            if (IsNew) // Si la cotización es para añadir, la añadimos
            {
                CurrentCotizacion.IdCotizacion = _sqlConnection.QueryFirst<long?>("INSERT INTO Cotizacion(Nombre, FechaRecepcion, FechaInicio, FechaTermino, IdInstitucion, IdSolicitante, IdResponsable, IdEstado, Comentarios) OUTPUT INSERTED.[IdCotizacion]  VALUES (@Nombre, @FechaRecepcion, @FechaInicio, @FechaTermino, @IdInstitucion, @IdSolicitante, @IdResponsable, @IdEstado, @Comentarios);",
                    new
                    {
                        CurrentCotizacion.Nombre,
                        CurrentCotizacion.FechaRecepcion,
                        CurrentCotizacion.FechaInicio,
                        CurrentCotizacion.FechaTermino,
                        CurrentCotizacion.Institucion.IdInstitucion,
                        CurrentCotizacion.Solicitante?.IdSolicitante,
                        CurrentCotizacion.Responsable?.IdResponsable,
                        CurrentCotizacion.Estado.IdEstado,
                        CurrentCotizacion.Comentarios
                    });

                foreach (Archivo archivo in CurrentCotizacion.Archivos)
                {
                    _sqlConnection.Execute(
                        "INSERT INTO dbo.Archivo(Nombre, Filename, Lineas, Comentarios, IdCotizacion) VALUES (@Nombre, @Filename, @Lineas, @Comentarios, @IdCotizacion); ",
                        new
                        {
                            archivo.Nombre,
                            archivo.Filename,
                            archivo.Lineas,
                            archivo.Comentarios,
                            CurrentCotizacion.IdCotizacion
                        });
                    
                }

            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
