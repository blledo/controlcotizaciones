﻿using System.ComponentModel;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using Dapper.Contrib.Extensions;

namespace ControlCotizaciones
{
    public class Archivo : INotifyPropertyChanged
    {
        private long? _idArchivo { get; set; }

        public long? IdArchivo
        {
            get => _idArchivo;
            set
            {
                _idArchivo = value;
                OnPropertyChanged();
            }
        }
        
        private string _nombre { get; set; }

        public string Nombre
        {
            get => _nombre;
            set
            {
                _nombre = value;
                OnPropertyChanged();
            }
        }
        
        private int _lineas { get; set; }

        public int Lineas
        {
            get => _lineas;
            set
            {
                _lineas = value;
                OnPropertyChanged();
            }
        }

        private string _filename { get; set; }
        
        public string Filename
        {
            get => _filename;
            set
            {
                _filename = value;
                OnPropertyChanged();
            }
        }

        private string _comentarios { get; set; }
        
        public string Comentarios
        {
            get => _comentarios;
            set
            {
                _comentarios = value;
                OnPropertyChanged();
            }
        }

        private Cotizacion _cotizacion { get; set; }

        public Cotizacion Cotizacion
        {
            get => _cotizacion;
            set
            {
                _cotizacion = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}