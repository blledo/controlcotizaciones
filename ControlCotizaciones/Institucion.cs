﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;

namespace ControlCotizaciones
{
    public class Institucion : AttributeObject
    {
        private long? _idInstitucion { get; set; }

        [Computed]
        public long? IdInstitucion
        {
            get => _idInstitucion;
            set
            {
                _idInstitucion = value;
               OnPropertyChanged();
            }
        }

        
        private string _nombre { get; set; }

        public string Nombre
        {
            get => _nombre;
            set
            {
                _nombre = value;
                OnPropertyChanged();
            }
        }

        private static SqlConnection _sqlConnection { get; set; } = new SqlConnection()
        {
            ConnectionString = Properties.Settings.Default.Database
        };

        public Institucion()
        {

        }
        /// <summary>
        /// Constructor que recibe un parámetro de nombre. Generalmente se usa para crear objetos de institución temporales,
        /// sin identificador en la base de datos.
        /// </summary>
        /// <param name="nombre"></param>
        public Institucion(string nombre)
        {
            Nombre = nombre;
        }
    }
}
