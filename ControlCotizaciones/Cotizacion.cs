﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ControlCotizaciones
{
    public class Cotizacion : INotifyPropertyChanged
    {
        private long? _idCotizacion { get; set; }

        public long? IdCotizacion
        {
            get => _idCotizacion;
            set
            {
                _idCotizacion = value;
                OnPropertyChanged();
            }
        }

        private string _nombre;
        public string Nombre
        {
            get
            {
                return _nombre;
            }
            set
            {
                _nombre = value;
                OnPropertyChanged();
            }
        }
        public DateTime FechaRecepcion { get; set; } = DateTime.Now;
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaTermino { get; set; }
        
        private Institucion _institucion { get; set; }

        public Institucion Institucion
        {
            get => _institucion;
            set
            {
                _institucion = value;
                OnPropertyChanged();
            }
        }
        public Solicitante Solicitante { get; set; }
        public Estado Estado { get; set; }
        public Responsable Responsable { get; set; }
        public string Comentarios { get; set; }
        
        private ObservableCollection<Archivo> _archivos { get; set; }

        public ObservableCollection<Archivo> Archivos
        {
            get => _archivos;
            set
            {
                _archivos = value;
                OnPropertyChanged();
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
