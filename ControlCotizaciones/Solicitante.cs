﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace ControlCotizaciones
{
    public class Solicitante
    {
        [Computed]
        public long? IdSolicitante { get; set; }
        public string Nombre { get; set; }

        public Solicitante()
        {

        }
        /// <summary>
        ///  Constructor con parámetro de nombre que generalmente se usa para Solicitantes temporales sin Id en la base de datos
        /// </summary>
        /// <param name="nombre"></param>
        public Solicitante(string nombre)
        {
            Nombre = nombre;
        }
    }
}
