﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace ControlCotizaciones
{
    public class Estado
    {
        [Computed]
        public long? IdEstado { get; set; }
        public string Nombre { get; set; }


        public Estado()
        {

        }
        /// <summary>
        /// Constructor con parámetro de descripción, que generalmente se usa cuando se crean estados temporales sin id en la base de datos
        /// </summary>
        /// <param name="nombre"></param>
        public Estado(string nombre)
        {
            Nombre = nombre;
        }
    }
}
