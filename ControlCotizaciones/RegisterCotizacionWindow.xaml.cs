﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Linq;
using Microsoft.Win32;

namespace ControlCotizaciones
{

    public partial class RegisterCotizacionWindow : Window
    {
        public RegisterCotizacionWindow()
        {
            InitializeComponent();           

            Loaded += RegisterCotizacionWindow_Loaded;
            
            // Creamos las claves en el registro que añaden el item de menú contextual para Explorer
 
            RegistryKey regmenu = null;
            RegistryKey regcmd = null;
            RegistryKey regicon = null;
            
            try
            {
                regmenu = Registry.CurrentUser.CreateSubKey(@"Software\Classes\*\shell\Liberalia");
                regmenu?.SetValue("","Registrar cotizacion...");
                regcmd = Registry.CurrentUser.CreateSubKey(@"Software\Classes\*\shell\Liberalia\command");
                regcmd?.SetValue("", $"{System.Reflection.Assembly.GetExecutingAssembly().Location} \"%1\"");
                regicon = Registry.CurrentUser.CreateSubKey(@"Software\Classes\*\shell\Liberalia\Icon");
                regicon?.SetValue("", System.Reflection.Assembly.GetExecutingAssembly().Location);

            }
            finally       
            {
                regmenu?.Close();
                regcmd?.Close();
                regicon?.Close();
            }       
        }

        /// <summary>
        /// Constructor con parámetros,  que recibe el path de una cotización existente
        /// </summary>
        /// <param name="path"></param>
        public RegisterCotizacionWindow(string path):
            this()
        {
            RegisterCotizacionViewModel registerCotizacionViewModel = DataContext as RegisterCotizacionViewModel;

            (ArchivosDataGrid.ItemsSource as ObservableCollection<Archivo>)?.Clear();
            registerCotizacionViewModel.CotizacionInfoFromFile(path);
        }

        private void RegisterCotizacionWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // A través de este evento, que se dispara cuando los templates ya se inicializaron, accedemos al templateBase TextBox
            // de los comboBox, y les damos CharacterCasing Upper, para asegurarnos que cualquier input sea en mayúsculas

            (InstitucionComboBox.Template.FindName("PART_EditableTextBox", InstitucionComboBox) as TextBox).CharacterCasing = CharacterCasing.Upper;
            (SolicitanteComboBox.Template.FindName("PART_EditableTextBox", SolicitanteComboBox) as TextBox).CharacterCasing = CharacterCasing.Upper;

            InstitucionComboBox.Focus();
        }

        /// <summary>
        /// El handler del comando para añadir la cotización a la base de datos. Bindeado con el botón de Añadir
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddComandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            RegisterCotizacionViewModel registerCotizacionViewModel = DataContext as RegisterCotizacionViewModel;
            if(ResponsableComboBox.SelectedItem == null)
            {
                (DataContext as RegisterCotizacionViewModel).CurrentCotizacion.Responsable = String.IsNullOrEmpty(ResponsableComboBox.Text) ? null : new Responsable(ResponsableComboBox.Text);

            }

            if (SolicitanteComboBox.SelectedItem == null)
            {
                (DataContext as RegisterCotizacionViewModel).CurrentCotizacion.Solicitante = String.IsNullOrEmpty(SolicitanteComboBox.Text) ? null : new Solicitante(SolicitanteComboBox.Text);
            }
            
            if(InstitucionComboBox.SelectedItem == null)
            {
                (DataContext as RegisterCotizacionViewModel).CurrentCotizacion.Institucion = new Institucion(InstitucionComboBox.Text);
            }
            
            if(EstadoComboBox.SelectedItem == null)
            {
                (DataContext as RegisterCotizacionViewModel).CurrentCotizacion.Estado = new Estado(EstadoComboBox.Text);

            }




            (DataContext as RegisterCotizacionViewModel).Submit();

            Application.Current.Shutdown();
        }
    }
}
