﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace ControlCotizaciones
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        [STAThread]
        public static void Main(string[] args)
        {
            /* Acá revisamos que argumentos se le dieron al iniciar la aplicación. Si se le entrega argumentos
             * de filename, entonces el usuario quiere añadir una cotización con archivo existente
             * */

            App app = new App();

            RegisterCotizacionWindow registerCotizacionWindow = null;
                
            registerCotizacionWindow = args.Length > 0 ? new RegisterCotizacionWindow(args[0]) : new RegisterCotizacionWindow();
             

            app.Run(registerCotizacionWindow);
        }
    }
}
