﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace ControlCotizaciones
{
    public class Responsable
    {
        [Computed]
        public long? IdResponsable { get; set; }
        public string Nombre { get; set; }

        public Responsable()
        {

        }
        /// <summary>
        /// Constructor con parametro de nombre, que se usa generalmente cuando se crea un responsable temporal sin ID
        /// </summary>
        /// <param name="name"></param>
        public Responsable(string nombre)
        {
            Nombre = nombre;
        }

    }
}
